<section id="videos">
	
	<div class="row">
		<div class="col-md-3 sin-padding">
			<div class="bloque-video">
				<a data-fancybox data-width="1280" data-height="720" href="recursos/video/pantalla.mp4">
					<video class="videosgaleria" width="100%" height="300" autoplay loop muted>
						<source src="recursos/video/pantalla.mp4" type="video/mp4">
					</video>
				</a>
				<h2>Entorno NK</h2>
			</div>
		</div>
		
		<div class="col-md-3 sin-padding">
			<div class="bloque-video">
				<a data-fancybox data-width="1280" data-height="720" href="recursos/video/video2.mp4">
					<video class="videosgaleria" width="100%" height="300" autoplay loop muted>
						<source src="recursos/video/video2.mp4" type="video/mp4">
					</video>
				</a>
				<h2>Recorrido Virtual</h2>
			</div>
		</div>
		
		
		
		
		<div class="col-md-3 sin-padding">
			<div class="bloque-video">
				<a data-fancybox data-width="1280" data-height="720" href="recursos/video/video1.mp4">
					<video class="videosgaleria" width="100%" height="300" autoplay loop muted>
						<source src="recursos/video/video1.mp4" type="video/mp4">
					</video>
				</a>
				<h2>Cristales Laminados</h2>
			</div>
		</div>
		<div class="col-md-3 sin-padding">
			<div class="bloque-video">
				<a data-fancybox data-width="1280" data-height="720" href="recursos/video/video3.mp4">
					<video class="videosgaleria" width="100%" height="300" autoplay loop muted>
						<source src="recursos/video/video3.mp4" type="video/mp4">
					</video>
				</a>
				<h2>Nueva Línea 7 de Metro</h2>
			</div>
		</div>
		
	</div>
</section>