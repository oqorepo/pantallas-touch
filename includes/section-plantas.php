<section id="plantas">
	<div id="todas-plantas">
		<div class="col-md-3 bloque-plantas" data-planta="modelo1">
			<img src="recursos/img/plantas/planta_1.png" class="img-responsive"/>
			<div class="descripcion">
				<h4>Depto Tipo A</h4>
				


<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num2.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num2.svg" />
	</li>
</div>


				<h4>Total 87.45 m²</h4>
				
			</div>
		</div>
		<div class="col-md-3 bloque-plantas" data-planta="modelo2">
			<img src="recursos/img/plantas/planta_2.png" class="img-responsive"/>
			<div class="descripcion">
				<h4>Depto Tipo B</h4>
			
<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num2.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num2.svg" />
	</li>
</div>
				<h4>Total 73.43 m²</h4>
		
			</div>
		</div>
		<div class="col-md-3 bloque-plantas" data-planta="modelo3">
			<img src="recursos/img/plantas/planta_3.png" class="img-responsive"/>
			<div class="descripcion">
				<h4>Depto Tipo C</h4>
			
<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num1.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num1.svg" />
	</li>
</div>
				<h4>Total 48.70 m²</h4>
			
			</div>
		</div>
		<div class="col-md-3 bloque-plantas" data-planta="modelo4">
			<img src="recursos/img/plantas/planta_4.png" class="img-responsive"/>
			<div class="descripcion">
				<h4>Depto Tipo D</h4>
			
<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num1.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num1.svg" />
	</li>
</div>
				<h4><p>Total 44.18 m²</p></h4>
		
			</div>
		</div>
	</div>
	<!-- Planta Single -->
	<!-- ----------------------------------------------------- -->
	<div class="single-plantas" data-planta="modelo1">
		<div class="columna-izquierda">
			<h4>Depto Tipo A</h4>

<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num2.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num2.svg" />
	</li>
</div>



			<h3>Total 87.45 m²</h3>
			<ul class="terminaciones_des">
				<li>Aire Acondicionado.</li>
				<li>Barandas de cristal templados, con lámina estructural anti-huracanes.</li>
				<li>Cubiertas de Cuarzo Españolas Silestone, única con protección anti-bacterias.</li>
				<li>Grifería Alemana Hans Groe.</li>
				<li>Quincallería Italiana AGB.</li>
				<li>Revestimientos de Piso y Muros de Porcelanatos.</li>
				<li>Aluminios Schüco, con tecnología que hace más eficiente el aislamiento térmico.</li>
				<li>Muebles de Cocina Leicht, fabricados y armados en Alemania.</li>
				<li>Piso Haro Alemán.</li>
			</ul>
		</div>
		<div class="columna-centro">
			<img src="recursos/img/plantas/planta_1.png" class="img-responsive"/>
		</div>
		<div class="columna-derecha">
			<div class="info" style="display: block;">
				<p class="superficie">Superficie Útil: <span>68.93 m²</span></p>
				<p class="superficie">Superficie Terraza: <span>18.52 m²</span></p>
				<!-- <p class="superficie">Superficie Logia: 5,65 m²</p> -->
						<p class="superficie">Superficie Total: <span>87.45 m²</span></p>
		
			</div>
			<img src="recursos/img/plantas/planta_1_info.png" class="img-responsive" />
		</div>
	</div>
	<!-- Planta Single -->
	<!-- ----------------------------------------------------- -->
	<div class="single-plantas" data-planta="modelo2">
		<div class="columna-izquierda">
			<h4>Depto Tipo B</h4>
<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num2.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num2.svg" />
	</li>
</div>
			<h3>Total 73.43 m²</h3>
			<ul class="terminaciones_des">
				<li>Aire Acondicionado.</li>
				<li>Barandas de cristal templados, con lámina estructural anti-huracanes.</li>
				<li>Cubiertas de Cuarzo Españolas Silestone, única con protección anti-bacterias.</li>
				<li>Grifería Alemana Hans Groe.</li>
				<li>Quincallería Italiana AGB.</li>
				<li>Revestimientos de Piso y Muros de Porcelanatos.</li>
				<li>Aluminios Schüco, con tecnología que hace más eficiente el aislamiento térmico.</li>
				<li>Muebles de Cocina Leicht, fabricados y armados en Alemania.</li>
				<li>Piso Haro Alemán.</li>
			</ul>
		</div>
		<div class="columna-centro">
			<img src="recursos/img/plantas/planta_2.png" class="img-responsive"/>
		</div>
		<div class="columna-derecha">
			<div class="info" style="display: block;">
				<p class="superficie">Superficie Útil: <span>60.33 m²</span></p>
				<p class="superficie">Superficie Terraza: <span>13.1 m²</span></p>
				<!-- <p class="superficie">Superficie Logia: 5,65 m²</p> -->
					<p class="superficie">Superficie Total: <span>73.43 m²</span></p>
			
			</div>
			<img src="recursos/img/plantas/planta_2_info.png" class="img-responsive" />
		</div>
	</div>
	<!-- Planta Single -->
	<!-- ----------------------------------------------------- -->
	<!-- Planta Single -->
	<!-- ----------------------------------------------------- -->
	<div class="single-plantas" data-planta="modelo3">
		<div class="columna-izquierda">
			<h4>Depto Tipo C</h4>
	<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num1.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num1.svg" />
	</li>
</div>
			<h3>Total 48.70 m²</h3>
			<ul class="terminaciones_des">
				<li>Aire Acondicionado.</li>
				<li>Barandas de cristal templados, con lámina estructural anti-huracanes.</li>
				<li>Cubiertas de Cuarzo Españolas Silestone, única con protección anti-bacterias.</li>
				<li>Grifería Alemana Hans Groe.</li>
				<li>Quincallería Italiana AGB.</li>
				<li>Revestimientos de Piso y Muros de Porcelanatos.</li>
				<li>Aluminios Schüco, con tecnología que hace más eficiente el aislamiento térmico.</li>
				<li>Muebles de Cocina Leicht, fabricados y armados en Alemania.</li>
				<li>Piso Haro Alemán.</li>
			</ul>
		</div>
		<div class="columna-centro">
			<img src="recursos/img/plantas/planta_3.png" class="img-responsive"/>
		</div>
		<div class="columna-derecha">

<div class="col-md-6">
	<h3>Tipo C1</h3>
	<div class="info" style="display: block;">
		<p class="superficie">Superficie Útil: <span>41.21 m²</span></p>
		<p class="superficie">Superficie Terraza: <span>10.7 m²</span></p>
		<!-- <p class="superficie">Superficie Logia: 5,65 m²</p> -->
		<p class="superficie">Superficie Total: <span>51.91 m²</span></p>
		
	</div>
</div>

<div class="col-md-6">
	<h3>Tipo C2</h3>
	<div class="info" style="display: block;">
		<p class="superficie">Superficie Útil: <span>41.21 m²</span></p>
		<p class="superficie">Superficie Terraza: <span>7.5 m²</span></p>
		<!-- <p class="superficie">Superficie Logia: 5,65 m²</p> -->
		<p class="superficie">Superficie Total: <span>48.7 m²</span></p> 
		
	</div>
</div>


			<img src="recursos/img/plantas/planta_3_info.png" class="img-responsive" />
		</div>
	</div>
	<!-- Planta Single -->
	<!-- ----------------------------------------------------- -->
	<!-- Planta Single -->
	<!-- ----------------------------------------------------- -->
	<div class="single-plantas" data-planta="modelo4">
		<div class="columna-izquierda">
			<h4>Depto Tipo D</h4>
<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num1.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num1.svg" />
	</li>
</div>
			<h3>Total 44.18 m²</h3>
			<ul class="terminaciones_des">
				<li>Aire Acondicionado.</li>
				<li>Barandas de cristal templados, con lámina estructural anti-huracanes.</li>
				<li>Cubiertas de Cuarzo Españolas Silestone, única con protección anti-bacterias.</li>
				<li>Grifería Alemana Hans Groe.</li>
				<li>Quincallería Italiana AGB.</li>
				<li>Revestimientos de Piso y Muros de Porcelanatos.</li>
				<li>Aluminios Schüco, con tecnología que hace más eficiente el aislamiento térmico.</li>
				<li>Muebles de Cocina Leicht, fabricados y armados en Alemania.</li>
				<li>Piso Haro Alemán.</li>
			</ul>
		</div>
		<div class="columna-centro">
			<img src="recursos/img/plantas/planta_4.png" class="img-responsive"/>
		</div>
		<div class="columna-derecha">
			<div class="info" style="display: block;">
				<p class="superficie">Superficie Útil: <span>36.06 m²</span></p>
				<p class="superficie">Superficie Terraza: <span>8.12 m²</span></p>
				<!-- <p class="superficie">Superficie Logia: 5,65 m²</p> -->
				<p class="superficie">Superficie Total: <span>44.18 m²</span></p>
				
			</div>
			<img src="recursos/img/plantas/planta_4_info.png" class="img-responsive" />
		</div>
	</div>
	<!-- Planta Single -->
	<!-- ----------------------------------------------------- -->
	<div id="plantas-miniaturas">
		<div id="mas-modelos">
			<!--  -->
			<div class="mas-planta">
				<div class="mas-planta-abierta">
					<div class="bajar-planta">
						<img src="recursos/img/next.svg" class="icon-svg" />
					</div>
					<img src="recursos/img/plantas/planta_1.png" class="img-responsive img-planta-abierta" />
					<div class="descripcion">
						<h4>Depto Tipo A</h4>
						<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num2.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num2.svg" />
	</li>
</div>
					</div>
					<div class="ver-planta" data-planta="modelo1">
						VER PLANTA
					</div>
				</div>
				<img src="recursos/img/plantas/planta_1.png" class="img-responsive img-mas-planta"/>
			</div>
			<!--  -->
			<!--  -->
			<div class="mas-planta" >
				<div class="mas-planta-abierta">
					<div class="bajar-planta">
						<img src="recursos/img/next.svg" class="icon-svg" />
					</div>
					<img src="recursos/img/plantas/planta_2.png" class="img-responsive img-planta-abierta" />
					<div class="descripcion">
						<h4>Depto Tipo B</h4>
											<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num2.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num2.svg" />
	</li>
</div>
					</div>
					<div class="ver-planta" data-planta="modelo2">
						VER PLANTA
					</div>
				</div>
				<img src="recursos/img/plantas/planta_2.png" class="img-responsive img-mas-planta"/>
			</div>
			<!--  -->
			<!--  -->
			<div class="mas-planta" >
				<div class="mas-planta-abierta">
					<div class="bajar-planta">
						<img src="recursos/img/next.svg" class="icon-svg" />
					</div>
					<img src="recursos/img/plantas/planta_3.png" class="img-responsive img-planta-abierta" />
					<div class="descripcion">
						<h4>Depto Tipo C</h4>
						<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num1.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num1.svg" />
	</li>
</div>
					</div>
					<div class="ver-planta" data-planta="modelo3">
						VER PLANTA
					</div>
				</div>
				<img src="recursos/img/plantas/planta_3.png" class="img-responsive img-mas-planta"/>
			</div>
			<!--  -->
			<!--  -->
			<div class="mas-planta" >
				<div class="mas-planta-abierta">
					<div class="bajar-planta">
						<img src="recursos/img/next.svg" class="icon-svg" />
					</div>
					<img src="recursos/img/plantas/planta_4.png" class="img-responsive img-planta-abierta" />
					<div class="descripcion">
						<h4>Depto Tipo D</h4>
						<div class="numeros">
	<li>
		<span class="dormitorios">Dorm</span>
		<img src="recursos/img/num1.svg" />
	</li>
	<li>
		<span class="dormitorios">Baños</span>
		<img src="recursos/img/num1.svg" />
	</li>
</div>
					</div>
					<div class="ver-planta" data-planta="modelo4">
						VER PLANTA
					</div>
				</div>
				<img src="recursos/img/plantas/planta_4.png" class="img-responsive img-mas-planta"/>
			</div>
			<!--  -->
		</div>
	</div>
</section>