<section id="galeria" class="section section-no-border background-color-light m-0 pb-0">
	<!-- start: .slick -->
	<div class="slick desktop galeria-lightbox">
		<div class="item">
			<div class="masonry">
				<div class="columna">
					<div class="grid-item w40 h80">
						<a href="recursos/img/galeria/galeria_01.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_01.jpg) no-repeat;"><img src="recursos/img/galeria/40x40.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w60 h80">
						<a href="recursos/img/galeria/galeria_02.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_02.jpg) no-repeat;"><img src="recursos/img/galeria/60x40.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h60">
						<a href="recursos/img/galeria/galeria_03.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_03.jpg) no-repeat;"><img src="recursos/img/galeria/100x60.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h60">
						<a href="recursos/img/galeria/galeria_000.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_000.jpg) no-repeat;"><img src="recursos/img/galeria/100x60.png" class="fullwidth"></a>
					</div>
				</div>
				<div class="columna right">
					<div class="grid-item w100 h60">
						<a href="recursos/img/galeria/galeria_04.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_04.jpg) no-repeat;"><img src="recursos/img/galeria/100x60-b.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h40">
						<a href="recursos/img/galeria/galeria_05.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_05.jpg) no-repeat;"><img src="recursos/img/galeria/100x40.png" class="fullwidth"></a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="item">
			<div class="masonry">
				<div class="columna">
					<div class="grid-item w33 h40">
						<a href="recursos/img/galeria/galeria_08.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_08.jpg) no-repeat;"><img src="recursos/img/galeria/a-30x40.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w66 h40">
						<a href="recursos/img/galeria/galeria_09.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_09.jpg) no-repeat;"><img src="recursos/img/galeria/a-70x40.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w50 h60">
						<a href="recursos/img/galeria/galeria_06.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_06.jpg) no-repeat;"><img src="recursos/img/galeria/a-50x60.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w50 h60">
						<a href="recursos/img/galeria/galeria_07.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_07.jpg) no-repeat;"><img src="recursos/img/galeria/a-50x60.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h60">
						<a href="recursos/img/galeria/galeria_03.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_03.jpg) no-repeat;"><img src="recursos/img/galeria/100x60.png" class="fullwidth"></a>
					</div>
				</div>
				<div class="columna right">
					<div class="grid-item w100 h50">
						<a href="recursos/img/galeria/galeria_10.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_10.jpg) no-repeat;"><img src="recursos/img/galeria/b-100x50.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h50">
						<a href="recursos/img/galeria/galeria_11.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_11.jpg) no-repeat;"><img src="recursos/img/galeria/b-100x50.png" class="fullwidth"></a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="item">
			<div class="masonry">
				<div class="columna">
					<div class="grid-item w50 h40">
						<a href="recursos/img/galeria/galeria_12.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_12.jpg) no-repeat;"><img src="recursos/img/galeria/a-50x60.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w50 h40">
						<a href="recursos/img/galeria/galeria_13.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_13.jpg) no-repeat;"><img src="recursos/img/galeria/a-50x60.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w66 h60">
						<a href="recursos/img/galeria/galeria_14.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_14.jpg) no-repeat;"><img src="recursos/img/galeria/a-70x40.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w33 h60">
						<a href="recursos/img/galeria/galeria_15.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_15.jpg) no-repeat;"><img src="recursos/img/galeria/a-30x40.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h60">
						<a href="recursos/img/galeria/galeria_03.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_03.jpg) no-repeat;"><img src="recursos/img/galeria/100x60.png" class="fullwidth"></a>
					</div>
				</div>
				<div class="columna right">
					<div class="grid-item w100 h50">
						<a href="recursos/img/galeria/galeria_16.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_16.jpg) no-repeat;"><img src="recursos/img/galeria/b-100x50.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h50">
						<a href="recursos/img/galeria/galeria_17.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_17.jpg) no-repeat;"><img src="recursos/img/galeria/b-100x50.png" class="fullwidth"></a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="item">
			<div class="masonry">
				<div class="columna">
					<div class="grid-item w50 h40">
						<a href="recursos/img/galeria/galeria_18.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_18.jpg) no-repeat;"><img src="recursos/img/galeria/a-50x60.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w50 h40">
						<a href="recursos/img/galeria/galeria_19.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_19.jpg) no-repeat;"><img src="recursos/img/galeria/a-50x60.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w66 h60">
						<a href="recursos/img/galeria/galeria_20.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_20.jpg) no-repeat;"><img src="recursos/img/galeria/a-70x40.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w33 h60">
						<a href="recursos/img/galeria/galeria_21.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_21.jpg) no-repeat;"><img src="recursos/img/galeria/a-30x40.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h60">
						<a href="recursos/img/galeria/galeria_03.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_03.jpg) no-repeat;"><img src="recursos/img/galeria/100x60.png" class="fullwidth"></a>
					</div>
				</div>
				<div class="columna right">
					<div class="grid-item w100 h50">
						<a href="recursos/img/galeria/galeria_22.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_22.jpg) no-repeat;"><img src="recursos/img/galeria/b-100x50.png" class="fullwidth"></a>
					</div>
					<div class="grid-item w100 h50">
						<a href="recursos/img/galeria/galeria_23.jpg" data-fancybox="gallery" class="lightbox mfp-image" style="background: url(recursos/img/galeria/galeria_23.jpg) no-repeat;"><img src="recursos/img/galeria/b-100x50.png" class="fullwidth"></a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<!-- end: .slick -->
</section>