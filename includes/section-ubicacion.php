<section id="ubicacion">
    <div id="activmap-wrapper">
        <!-- Places panel (auto removable) -->
        <div id="activmap-places">
            <div id="activmap-results-num"></div>
        </div>
        <!-- Activ'Map global wrapper -->
        <div id="activmap-container">
            <!-- Toolbar -->
            <div id="activmap-ui-wrapper">
                <!-- Activ'Map categories and tags -->
                <div id="activmap-filters">
                    <a class="activmap-action hide" id="activmap-reset" href="#"></a>
                    <div class="marker-selector">
                        <!-- Add checked="checked" to show the markers of this filter on page loading -->
                        <input type="checkbox" name="marker_type[]" value="tag_mall" id="tag_mall">
                        <label for="tag_mall"><img src="recursos/img/logos/1.svg" class="map-svg"/><div class="map-nombre">Mall</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_hotel" id="tag_hotel">
                        <label for="tag_hotel"><img src="recursos/img/logos/2.svg" class="map-svg"/><div class="map-nombre">Hotel</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_boulevard" id="tag_boulevard">
                        <label for="tag_boulevard"><img src="recursos/img/logos/3.svg" class="map-svg"/><div class="map-nombre">Boulevard</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_colegios" id="tag_colegios">
                        <label for="tag_colegios"><img src="recursos/img/logos/4.svg" class="map-svg"/><div class="map-nombre">Colegios</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_servicentros" id="tag_servicentros">
                        <label for="tag_servicentros"><img src="recursos/img/logos/5.svg" class="map-svg"/><div class="map-nombre">Servicentros</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_parques" id="tag_parques">
                        <label for="tag_parques"><img src="recursos/img/logos/6.svg" class="map-svg"/><div class="map-nombre">Parques</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_supermercados" id="tag_supermercados">
                        <label for="tag_supermercados"><img src="recursos/img/logos/7.svg" class="map-svg"/><div class="map-nombre">Supermercados</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_bancos" id="tag_bancos">
                        <label for="tag_bancos"><img src="recursos/img/logos/8.svg" class="map-svg"/><div class="map-nombre">Bancos</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_clinicas" id="tag_clinicas">
                        <label for="tag_clinicas"><img src="recursos/img/logos/9.svg" class="map-svg"/><div class="map-nombre">Clínicas</div></label>
                    </div>
                    <div class="marker-selector">
                        <input type="checkbox" name="marker_type[]" value="tag_restaurantes" id="tag_restaurantes">
                        <label for="tag_restaurantes"><img src="recursos/img/logos/10.svg" class="map-svg"/><div class="map-nombre">Restaurantes</div></label>
                    </div>
                </div>
            </div>
            <!-- Map container REQUIRED -->
            <div id="activmap-canvas"></div>
        </div>
    </div>
</section>