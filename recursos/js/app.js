$(window).on('load', function() {
   // Carga Libreria Slick
   $('#galeria div.slick').slick({
        cssEase: 'ease',
        fade: false,
        arrows: true,
        dots: false,
        swipe: true,
        centerPadding: 0,
        focusOnSelect: false,
        centerMode: false,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        draggable: true,
    });
    $(".loading").fadeOut('slow');
    $(".videobersa").trigger('play');
});

// Define ruta
var urlapp = "http://localhost/NK/Dropbox/";



$(document).ready(function() {
// ---------------------------------------------------------------
// Función para comprobar si existe Internet
// ---------------------------------------------------------------
function compruebaInternet() {
    var i = new Image();
    i.onload = siHayInternet;
    i.onerror = noHayInternet;
    i.src = 'https://www.google.cl/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png?d=' + escape(Date());
}
function noHayInternet() {
    console.log('Status: Sin Internet');
    $('section.nohayinternet').fadeIn();

}
function siHayInternet() {
    console.log('Status: Con Internet')
}

// ---------------------------------------------------------------
// Función Botón Reset
// ---------------------------------------------------------------
var logoreset = document.getElementById("logoreset");
var timer, timeout = 400;
logoreset.addEventListener("dblclick", function (evt) {
    timer = setTimeout(function () {
        timer = null;
    }, timeout);
});
logoreset.addEventListener("click", function (evt) {
    if (timer) {
      console.log('Status: Reiniciando aplicación');
        clearTimeout(timer);
        timer = null;
        location.reload();
    }
});

// ---------------------------------------------------------------
// Función para comprobar inactividad en la pantalla
// ---------------------------------------------------------------
var timeoutID;
    function setup() {
        this.addEventListener("mousemove", resetTimer, false);
        this.addEventListener("mousedown", resetTimer, false);
        this.addEventListener("keypress", resetTimer, false);
        this.addEventListener("DOMMouseScroll", resetTimer, false);
        this.addEventListener("mousewheel", resetTimer, false);
        this.addEventListener("touchmove", resetTimer, false);
        this.addEventListener("MSPointerMove", resetTimer, false);
        startTimer();
    }
    setup();
    function startTimer() {
        // espera 8 minutos de inactividad y vuelve al inicio
        timeoutID = window.setTimeout(goInactive, 480000);
        // timeoutID = window.setTimeout(goInactive, 10000); // Pruebas
    }

    function resetTimer(e) {
        window.clearTimeout(timeoutID);
        goActive();
    }
    function goInactive() {
        console.log('Status: No hay actividad, se reinicia')
        $('section').fadeOut();
        $('#inicio-video').fadeIn();
        $(".videobersa").trigger('play');
        $('.menu li').removeClass('actual');
		$('.fancybox-button--close').trigger('click');
        if ($('.menu').hasClass('abierto')){
        	$('#menu-icon-trigger').trigger('click');
        }
        $('#inicio-menu').fadeIn();
    }
    function goActive() {
        startTimer();
    }


// ---------------------------------------------------------------
// Función para reproducir videos de forma automática
// ---------------------------------------------------------------
    $('.videos').on('click', function() {
        $('videosgaleria').trigger('play');
            console.log('Status: Reproduciendo videos de forma automática')
    });


// ---------------------------------------------------------------
// Función para la sección de Plantas
// ---------------------------------------------------------------
    $('.bloque-plantas').each(function() {
        $(this).on('click', function() {
            var planta = $(this).data('planta');
            console.log('Hizo Click en ' + planta);
            $('#todas-plantas').hide();
            $('.single-plantas[data-planta="' + planta + '"]').fadeIn();
            $('#plantas-miniaturas ').fadeIn();
            $('.ver-planta[data-planta="' + planta + '"]').parent().next('.img-mas-planta').addClass('actual');
        });
    });
    // Cuando se hace click en una planta miniatura
    $('.img-mas-planta').each(function() {
        $(this).on('click', function() {
            $('section.mascara').fadeIn('slow');
            $('.mas-planta-abierta').slideUp();
            $(this).prev('.mas-planta-abierta').slideDown();
            $(this).children('.img-mas-planta').fadeIn();
        });
    });
    // Cuando se hace click al bajar la planta en miniatura
    $('.bajar-planta').each(function() {
        $(this).on('click', function() {
            $('section.mascara').fadeOut('slow');
            $(this).parent('.mas-planta-abierta').slideUp();
        });
    });
    // Cuando se hace click en Ver Plantas
    $('.ver-planta').each(function() {
        $(this).on('click', function() {
            var planta = $(this).data('planta');
            $('.single-plantas').hide();
            $('section.mascara').fadeOut('slow');
            $(this).parent('.mas-planta-abierta').slideUp();
            $('.single-plantas[data-planta="' + planta + '"]').fadeIn();
            console.log('Hizo click en ver más en ' + planta);
            $('.img-mas-planta').removeClass('actual');
            $(this).parent('.mas-planta-abierta').next('.img-mas-planta').addClass('actual');
        });
    });
    // Cuando se hace click en la fotografía de la planta minuatura
    $('.img-planta-abierta').each(function() {
        $(this).on('click', function() {
            $(this).next().next().trigger('click');
        });
    });



// ---------------------------------------------------------------
// Función para reproducir videos de forma automática
// ---------------------------------------------------------------
    $('#inicio-menu').on('click', function() {
        $(this).toggle();
        $('.menu-icon-trigger').trigger('click');
        console.log('Status: Hizo click en el Inicio');

    });
    // ----------------------------------------------
    // -------------------- Menu --------------------
    // ----------------------------------------------
    $('.boton-menu').on('click', function() {
        $('.menu').toggleClass('abierto');
        $('#inicio-menu').hide();
        console.log('Status: Hizo click en el Inicio');

    });
    $('.menu li').each(function() {
        var section = $(this).data('section');
        $(this).on('click', function() {
            // Resetear Mapa

            // Resetea las opciones de las plantas
            $('#plantas-miniaturas').hide();
            $('#todas-plantas').show();
            $('.single-plantas').hide();
            $('.img-mas-planta').removeClass('actual');
            $('.menu li').removeClass('actual');
            $(this).addClass('actual');
            $('#menu-icon-trigger').trigger('click');
            $('section').hide();
            $('section#' + section).fadeIn();
            $(".videobersa").trigger('pause');



            if (section == "inicio-video") {
                $('#inicio-menu').fadeIn();
                $(".videobersa").trigger('play');


            }

            if (section == "galeria") {
            $(window).trigger('resize');
            }

            if (section == "ubicacion") {
                 compruebaInternet();
                 $(window).trigger('resize');
            }
            if (section == "video360") {
                compruebaInternet();
                $('section.video360').fadeIn();

            }


        });
    });


    // ----------------------------------------------
    // -------------------- Galerías --------------------
    // ----------------------------------------------

    $('#galeria div.lista-mobile div.grid').masonry({
        itemSelector: 'a.lightbox'
    });

    $('[data-fancybox="images"]').fancybox({
        afterLoad: function(instance, current) {
            var pixelRatio = window.devicePixelRatio || 1;

            if (pixelRatio > 1.5) {
                current.width = current.width / pixelRatio;
                current.height = current.height / pixelRatio;
            }
        }
    });


});




// ----------------------------------------------
// -------------------- MAPA --------------------
// ----------------------------------------------

$(function() {
    // Activ'Map plugin init
    // All options here: http://activmap.pandao.eu/doc/#options
    $('#activmap-wrapper').activmap({
        places: [{
            title: 'Parque Arauco',
            address: 'Av. Kennedy 5413, Las Condes',
            tags: ['tag_mall'],
            lat: -33.4022667,
            lng: -70.5811109,
            img: urlapp + 'recursos/img/lugares/parque-arauco.png',
            icon: urlapp + 'recursos/img/logos/1.png'
        }, {
            title: 'Alto Las Condes',
            address: 'Av. Pdte. Kennedy Lateral 9001, Las Condes',
            tags: ['tag_mall'],
            lat: -33.3908732,
            lng: -70.5484722,
            img: urlapp + 'recursos/img/lugares/alto-las-condes.png',
            icon: urlapp + 'recursos/img/logos/1.png'
        }, {
            title: 'Hotel Marriott Santiago',
            address: 'Av. Pdte. Kennedy 5741, Las Condes',
            tags: ['tag_hotel'],
            lat: -33.399976,
            lng: -70.5770677,
            img: urlapp + 'recursos/img/lugares/marriott.png',
            icon: urlapp + 'recursos/img/logos/2.png'
        }, {
            title: 'Hotel Atton Las Condes',
            address: 'Alonso de Córdova 5199, Las Condes',
            tags: ['tag_hotel'],
            lat: -33.4064176,
            lng: -70.5786085,
            img: urlapp + 'recursos/img/lugares/atton.png',
            icon: urlapp + 'recursos/img/logos/2.png'
        }, {
            title: 'Boulevard',
            address: 'Cerro Colorado 5612, Las Condes',
            tags: ['tag_boulevard'],
            lat: -33.404532,
            lng: -70.5784855,
            img: urlapp + 'recursos/img/lugares/boulevard.png',
            icon: urlapp + 'recursos/img/logos/3.png'
        }, {
            title: 'Colegio Alemán',
            address: 'Nuestra Señora del Rosario 850, Las Condes',
            tags: ['tag_colegios'],
            lat: -33.4001288,
            lng: -70.5695724,
            img: urlapp + 'recursos/img/lugares/colegio_aleman.png',
            icon: urlapp + 'recursos/img/logos/4.png'
        }, {
            title: 'Colegio Internacional Sek',
            address: 'Los Militares 6640, Las Condes',
            tags: ['tag_colegios'],
            lat: -33.4029368,
            lng: -70.5667414,
            img: urlapp + 'recursos/img/lugares/colegio_sek.png',
            icon: urlapp + 'recursos/img/logos/4.png'
        }, {
            title: 'Colegio Compañía de María Apoquindo',
            address: 'Av. Manquehue Sur 116, Las Condes',
            tags: ['tag_colegios'],
            lat: -33.4097828,
            lng: -70.5715664,
            img: urlapp + 'recursos/img/lugares/colegio_compania.png',
            icon: urlapp + 'recursos/img/logos/4.png'
        }, {
            title: 'Servicentro Copec',
            address: 'Av. Manquehue Norte., Las Condes',
            tags: ['tag_servicentros'],
            lat: -33.402088,
            lng: -70.5710231,
            img: urlapp + 'recursos/img/lugares/copec.png',
            icon: urlapp + 'recursos/img/logos/5.png'
        }, {
            title: 'Parque Araucano',
            address: 'Pdte. Riesco 5877, Las Condes',
            tags: ['tag_parques'],
            lat: -33.403118,
            lng: -70.571545,
            img: urlapp + 'recursos/img/lugares/parque_araucano.png',
            icon: urlapp + 'recursos/img/logos/6.png'
        }, {
            title: 'Supermercado Unimarc',
            address: 'Av. Manquehue Nte. 457, Las Condes',
            tags: ['tag_supermercados'],
            lat: -33.404080,
            lng: -70.568325,
            img: urlapp + 'recursos/img/lugares/unimarc.png',
            icon: urlapp + 'recursos/img/logos/7.png'
        }, {
            title: 'Supermercado Unimarc',
            address: 'Cerro El Plomo 5680, Piso 7, Las Condes',
            tags: ['tag_supermercados'],
            lat: -33.405231,
            lng: -70.573815,
            img: urlapp + 'recursos/img/lugares/unimarc2.png',
            icon: urlapp + 'recursos/img/logos/7.png'
        }, {
            title: 'Supermercado Tottus Kennedy',
            address: 'Av Pdte. Kennedy 5601, Las Condes',
            tags: ['tag_supermercados'],
            lat: -33.401180,
            lng: -70.575390,
            img: urlapp + 'recursos/img/lugares/tottus.png',
            icon: urlapp + 'recursos/img/logos/7.png'
        }, {
            title: 'Banco Santander',
            address: 'Av. Pdte. Kennedy Lateral 5413 - Local 501-A, Las Condes',
            tags: ['tag_bancos'],
            lat: -33.401946,
            lng: -70.580150,
            img: urlapp + 'recursos/img/lugares/santander.png',
            icon: urlapp + 'recursos/img/logos/8.png'
        }, {
            title: 'Clínica Alemana',
            address: '5951, Av Vitacura, Vitacura',
            tags: ['tag_clinicas'],
            lat: -33.392365,
            lng: -70.572959,
            img: urlapp + 'recursos/img/lugares/clinica_alemana.png',
            icon: urlapp + 'recursos/img/logos/9.png'
        }, {
            title: 'Restaurant Margó',
            address: 'Av. Pdte. Kennedy Lateral 5445, Las Condes',
            tags: ['tag_restaurantes'],
            lat: -33.401353,
            lng: -70.577859,
            img: urlapp + 'recursos/img/lugares/margo.png',
            icon: urlapp + 'recursos/img/logos/10.png'
        }, ],
        center_icon: urlapp + "/recursos/img/logo_bersa.png",
        icon: urlapp + "/recursos/img/logo_bersa.png",
        lat: -33.4005464, //latitude of the center
        lng: -70.572549, //longitude of the center
        posPanel: 'left',
        showPanel: true,
        radius: 0,
        zoom: 13,
        cluster: false,
        country: 'cl',
        mapType: 'roadmap',
        request: 'strict',
        allowMultiSelect: false,
        locationTypes: ['geocode', 'establishment'],
        styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#6d6d6d"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}],
    });
});