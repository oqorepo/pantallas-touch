<!DOCTYPE html>
<!--[if lte IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="not-ie no-js" lang="es"><!--<![endif]-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>App NK Touch</title>
	<meta name="author" content="Jorge Arancibia">
	<link rel="stylesheet" href="recursos/js/bootstrap-3.3.7-dist/css/bootstrap.min.css" crossorigin="anonymous">
	<link href="recursos/js/slick/slick.css" rel="stylesheet" type="text/css">
	<link href="recursos/js/slick/slick-theme.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=GFS+Didot" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300" rel="stylesheet">
	<link rel="stylesheet" href="recursos/js/fancybox-master/dist/jquery.fancybox.min.css">
	<link rel="stylesheet" href="recursos/css/galeria.css">
    <link rel="stylesheet" href="mapa/jquery-activmap/css/skin-compact/activmap-compact.css">
    <link rel="stylesheet" href="mapa/jquery-activmap/css/skin-compact/activmap-dark.css">
    	<link rel="stylesheet" href="recursos/css/offline.css">
	<link rel="stylesheet" href="recursos/css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Karla:400,700|Open+Sans:300,400,600,700|Prata" rel="stylesheet">
</head>
<body>

<!-- Secciones -->

<?php include('includes/loading.php');?>


<?php include('includes/header.php');?>
<?php include('includes/menu.php');?>
<section class="mascara"></section>

<?php include('includes/section-videos.php');?>


<?php include('includes/section-plantas.php');?>
<?php include('includes/section-galeria.php');?>
<?php include('includes/section-vista.php');?>

<?php include('includes/section-inicio.php');?>
<?php include('includes/section-ubicacion.php');?>


<?php include('includes/section-360.php');?>

<?php include ('includes/section-sininternet.php');?>

<!-- Librerías -->
<script src="recursos/js/ease.min.js"></script>
<script src="recursos/js/segment.min.js"></script>
<script src="recursos/js/menuicon.js" ></script>
<script src="recursos/js/jquery-3.3.1.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVEbTvImI1dWnKD9MA3k9pkReLpf0TIhU&libraries=places&language=es"></script>
<!--     <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&language=en"></script> -->


    <script src="mapa/jquery-activmap/js/jquery-activmap.min.js"></script>
    <script src="mapa/jquery-activmap/js/markercluster.min.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,300,700">
    <link rel="stylesheet" href="recursos/font/font-awesome-4.7.0/css/font-awesome.min.css">

<script src="recursos/js/fancybox-master/dist/jquery.fancybox.min.js"></script>
<script src="recursos/js/masonry.pkgd.min.js"></script>
<script src="recursos/js/slick/slick.js"></script>
<script src="recursos/js/offline.min.js"></script>
<script src="recursos/js/app.js"></script>
</body>
</html>